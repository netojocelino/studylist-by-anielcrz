#Listagem dos materiais de estudo de JS
---
Olá pessoal, segue abaixo uma listagem dos materiais de estudo que temos disponível para o aprendizado do **Javascript** e **Node.JS** 

##Treinamentos do PluralSight

* [Introdução ao Desenvolvimento WEB + Node](https://app.pluralsight.com/library/courses/web-development-intro/table-of-contents) Para quem ainda não conhece o funcionamento de desenvolvimento para Web, esse curso é bastante recomendado.
* [Introdução ao Desenvolvimento com Node.js](https://app.pluralsight.com/library/courses/node-intro/table-of-contents) Uma introdução geral à plataforma e seus componentes.
* [Introdução ao MongoDB](https://app.pluralsight.com/library/courses/mongodb-introduction/table-of-contents) Uma introdução ao MongoDB e o seu uso, pois é o banco de dados mais usado em conjunto com o Node.js.
* [Usando o MongodB com Node.JS via Mongoose](https://app.pluralsight.com/library/courses/mongoose-for-nodejs-mongodb/table-of-contents) Como usar o Node.JS + o MongoDB. 
* [Criando web apps com Node.jS](https://app.pluralsight.com/library/courses/building-web-apps-nodejs/table-of-contents) Como criar web apps simples, sem banco de dados, com o Node.JS
* [Criando API RESTfull com Node.js](https://app.pluralsight.com/library/courses/node-js-express-rest-web-services/table-of-contents) E este, é um dos mais importantes pois é a técnica que os nossos projetos de APIs são baseados.

Vejam esses treinamentos em conjunto com os matériais complementares abaixo:

---  

##Javascript

* [Enloquente Javascript](https://github.com/braziljs/eloquente-javascript) - -  Este livro é a versão traduzida do livro aberto da comunidade [EloquentJs](http://eloquentjavascript.net/), possui uma liguagem simples e é recomendado para aqueles que não tiveram contato com a linguagem.

* [JavaScript - The Deinitive Guide](https://drive.google.com/open?id=0B2xLodLWr2SNeE5FZ0dRTFpmRjQ&authuser=0) -- Livro em inglês bem completo sobre o JavaScript.

* [JavaScript - The Good Parts](https://drive.google.com/open?id=0B2xLodLWr2SNSGxzTnFFcWczZDg&authuser=0) -- Livro também em inglês que apresenta boas práticas no desenvolvimento na linguagem.

##Node.Js

* [Construindo APIs REST com Node.JS](https://drive.google.com/open?id=0B2xLodLWr2SNdDBSdzlxd0tzZUU) -- Livro da casa do código que auxilia no aprendizado de criação de APIs REST com o Node.JS.

* [Hands-On Node.JS](https://drive.google.com/open?id=0B2xLodLWr2SNbThRZDJzR0FCLTA) -- Primeiro livro lançado sobre o Node.JS e sempre traz novidades da plataforma em suas edições.

* [JavaScript and Node Fundamentals](https://drive.google.com/open?id=0B2xLodLWr2SNZndwM1JOOXVQcmc) -- Livro básico sobre Node.js e JavaScript.

* [ (Vídeo) Udemy - Learn NodeJs by 10 projects](https://drive.google.com/open?id=0B2xLodLWr2SNYUdXRjI5elFPODQ) -- Curso para aprendizado de Node.JS fazendo 10 projetos.

* [ (Vídeo) Udemy - The Complete Node Course](https://drive.google.com/open?id=0B2xLodLWr2SNYVVJd19nTzVtbGM) -- Curso para aprendizado de Node.JS desde o principio.

##Angular e outros materiais

* [Pasta dos materiais de estudo](https://drive.google.com/open?id=0B2xLodLWr2SNeGtlcnZjQkd1QzQ) -- Pasta geral com diversos materiais de estudo. (Aviso: Ela está sendo movida e categorizada. Quando tiver terminado o link será outro e este aviso sumirá).

* [45 Livros da casa do código](https://drive.google.com/open?id=0B2xLodLWr2SNcnp2S0xiZnNRV3M) -- Coletânea de livros da Casa do Código unicamente para estudos, evite compartilhar.